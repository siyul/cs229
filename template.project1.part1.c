/*
 * =====================================================================================
 *
 *       Filename:  computmat.c
 *
 *    Description:  Compute the score, deletion score, insertion score from 
 *    two DNA sequences using the dynamic programming. Produce the best alignment of two 
 *    sequences by tracing back.
 *
 *    1.0 I have  problems on the proalg and the getseq ; because they produce 
 *    some unknow code if DNA sequence are read from a file, but the test
 *    printf in the producealg seems good.
 *    The program is only tested by the A1.txt and the B1.txt.
 *    
 *    1.5 Now it works on sample sequences and produces correct results.
 *    Thanks Dr.Huang for providing the algorithm and some other helper
 *    funtions.
 *
 *    
 *
 *        Version:  1.5
 *        Created:  10/03/2014 08:45:03 PM
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin, 
 *         @author siyu lin
 *   Organization: Iowa State University 
 *
 * =====================================================================================
 */


#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit().
#include <string.h> // A library with strcpy(), strcmp(), strlen().
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define MaxLineLen 1000

struct seqtp   // a structure for three features of a sequence
 { char *name; // pointer to a string for keeping the name of a sequence
   char *seq;  // pointer to a char array for storing the sequence
   int  slen;  // the length of the sequence = char array length - 1,
 };            // where seq[1], seq[2], ... seq[n] are used
               // to keep the lettes of a sequence of length n.

struct mattp    // a structure for three matrices
 {  int  rind;  // max row index
    int  cind;  // max column index
    int  **Dpt; // an int pointer to 2-dimensional array D[rind + 1][cind + 1]
    int  **Ipt; // an int pointer to 2-dimensional array I[rind + 1][cind + 1]
    int  **Spt; // an int pointer to 2-dimensional array S[rind + 1][cind + 1]
 };

struct scoretp  // a structure for scoring parameters
 { int  match;  // a positive score for a pair of identical DNA letters
   int  mismat; // a negative score for a pair of different DNA letters
   int  gopen;  // a negative score for a gap
   int  gext;   // a negative score for each letter in a gap
 };
struct algtp    // a structure for an optimal alignment  
 { int  score;  // the score of the alignment
   int  alen;   // the length of the alignment
   char *top;   // pointer to a char array for the top row of the alignment
   char *mid;   // pointer to a char array for the mid row of the alignment
   char *bot;   // pointer to a char array for the bottom row of the alignment
 };              // each array is of length alen.

void fatal(const char *msg);
FILE *ckopen(const char *name, const char *mode);
void *check_malloc(size_t amount);
void *check_realloc(void *ptr, size_t amount);
void getseq(const char *fname, struct seqtp *seqrec);
void **get2dspace(int rind, int cind);
void computemats(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct mattp *matspt);
void outputmat(char kind, struct seqtp *sonept, struct seqtp *stwopt, int **Mpt);
void freeseq(struct seqtp *seqrec);
void free2dspace(int rind, int **arr);
void freemat(struct mattp *matspt);
void producealg(struct seqtp *sonept, struct seqtp *stwopt, struct mattp *matspt, struct algtp *algopt);
void freealg(struct algtp *algopt);
void outputalg(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct algtp *algopt);

int main(int argc, char *argv[]) 
{
    char *seqAFile;
    char *seqBFile;
    struct seqtp *seqApt;
    struct seqtp *seqBpt;
    struct scoretp defaultConfig;
    struct mattp *mat;
    struct algtp *alg;

   if ( argc != 6 )
   { fprintf(stderr,"Usage: %s Seq1 Seq2 mismatch gap_open gap_extend\n\n", argv[0]);
     fprintf(stderr,"Seq1        file of one sequence in FASTA format\n");
     fprintf(stderr,"Seq2        file of one sequence in FASTA format\n");
     fprintf(stderr,"mismatch    a negative integer\n");
     fprintf(stderr,"gap_open    gap open penalty, a non-negative integer \n");
     fprintf(stderr,"gap_extend  gap extension penalty, a positive integer \n");
     // Each base match score is always 10.
     exit(1);
   }

     sscanf(argv[3], "%d", &(defaultConfig.mismat));
     if(defaultConfig.mismat >= 0){
         fatal("Mismatch should be negative \n");
     }
     sscanf(argv[4], "%d", &(defaultConfig.gopen));
     if(defaultConfig.gopen <= 0){
         fatal("Gap-open should be positive \n");
     }
    sscanf(argv[5], "%d", &(defaultConfig.gext));
     if(defaultConfig.gext <= 0){
         fatal("Gap-Extension should be positive \n");
     }
    printf("%10s %10s %10s %10s\n", "Match Score", "Mismatch Score", "Gap-open", "Gap-extension");
    defaultConfig.match = 10; 
    printf("%10d %10d %10d %10d\n", defaultConfig.match, defaultConfig.mismat, defaultConfig.gopen, defaultConfig.gext);
    //Write sequence A to seqtp struct
    //


    seqApt =(struct seqtp*)check_malloc(sizeof(struct seqtp));    
    seqAFile=argv[1];

    //Write sequence A to seqtp struct

    seqBpt =(struct seqtp*)check_malloc(sizeof(struct seqtp));    
    seqBFile=argv[2];
    getseq(seqAFile, seqApt);
    getseq(seqBFile, seqBpt);


    mat = (struct mattp*)check_malloc(sizeof(struct mattp));
    alg = (struct algtp*)check_malloc(sizeof(struct algtp));

    computemats(seqApt,seqBpt,&defaultConfig,mat);
    producealg(seqApt,seqBpt,mat,alg);
    outputalg(seqApt,seqBpt,&defaultConfig,alg);
    //outputmat('S',seqApt,seqBpt,(*mat).Spt);
    printf("Score is %d\n", mat->Spt[0][0]);

    freeseq(seqApt);
    freeseq(seqBpt);
    free(seqApt);
    seqApt = NULL;
    free(seqBpt);
    seqBpt = NULL;
    freemat(mat);
    free(mat);
    mat = NULL;
    freealg(alg);
    free(alg);
    alg = NULL;
    return 0;
}


void freemat(struct mattp *matspt){
    int rind = matspt->rind;
    free2dspace(rind,matspt->Dpt); 
    free2dspace(rind,matspt->Ipt); 
    free2dspace(rind,matspt->Spt); 
}
void freeseq(struct seqtp *seqrec){
    free(seqrec->name);
    seqrec->name = NULL;
    free(seqrec->seq);
    seqrec->seq = NULL;
    seqrec->slen = 0;
}

void freealg(struct algtp *algopt){

    free(algopt->top);
    free(algopt->mid);
    free(algopt->bot);
    algopt->alen = 0;
    algopt->score = 0;
    algopt->top = NULL;
    algopt->mid = NULL;
    algopt->bot = NULL;

}
void computemats(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct mattp *matspt){
    int i;
    int j;
    int rind;
    int cind;
    inline int matchingScore(char x, char y) { 
        return x == y ? (*papt).match : (*papt).mismat;
    }
    (*matspt).rind = (*sonept).slen;
    rind = (*sonept).slen;
    (*matspt).cind = (*stwopt).slen;
    cind = (*stwopt).slen;

    (*matspt).Dpt = (int **)get2dspace(rind, cind); 
    (*matspt).Ipt = (int **)get2dspace(rind, cind); 
    (*matspt).Spt = (int **)get2dspace(rind, cind); 

    //Base cases
    (*matspt).Spt[rind][cind] = 0;
    (*matspt).Dpt[rind][cind] = (*matspt).Spt[rind][cind]-(*papt).gopen;
    (*matspt).Ipt[rind][cind] = (*matspt).Spt[rind][cind]-(*papt).gopen;
    //Initializing the last colomn
    for(i=rind-1; i >=0; i--)
    {
       (*matspt).Dpt[i][cind] = (*matspt).Dpt[i+1][cind]-(*papt).gext;
       (*matspt).Spt[i][cind] = (*matspt).Dpt[i][cind];
       (*matspt).Ipt[i][cind] = (*matspt).Spt[i][cind]-(*papt).gopen;
    }


    //Initializing the last row
    for(j=cind-1;j>=0; j--){
       (*matspt).Ipt[rind][j] = (*matspt).Ipt[rind][j+1]-(*papt).gext;
       (*matspt).Spt[rind][j] = (*matspt).Ipt[rind][j];
       (*matspt).Dpt[rind][j] = (*matspt).Spt[rind][j]-(*papt).gopen;
    }



    for(i = (*matspt).rind-1; i >= 0; i --){

       (*matspt).Dpt[i][cind] = (*matspt).Dpt[i+1][cind]-(*papt).gext;
       (*matspt).Spt[i][cind] = (*matspt).Dpt[i][cind];
       (*matspt).Ipt[i][cind] = (*matspt).Spt[i][cind]-(*papt).gopen;
        for(j = (*matspt).cind-1; j >= 0; j --){
            (*matspt).Dpt[i][j] = max(((*matspt).Dpt[i+1][j]-(*papt).gext),((*matspt).Spt[i+1][j]-(*papt).gext-(*papt).gopen));
            (*matspt).Ipt[i][j] = max(((*matspt).Ipt[i][j+1]-(*papt).gext),((*matspt).Spt[i][j+1]-(*papt).gext-(*papt).gopen));

            int diMax = max(((*matspt).Dpt[i][j]),((*matspt).Ipt[i][j]));
            char a = (*sonept).seq[i+1];
            char b = (*stwopt).seq[j+1];
            int matchingS = matchingScore(a, b);
            int s = ((*matspt).Spt[i+1][j+1])+matchingS;
            (*matspt).Spt[i][j] = max((s),(diMax));
        }
    }

}
void producealg(struct seqtp *sonept, struct seqtp *stwopt,  struct mattp *matspt, struct algtp *algopt){
    enum MatchType {S,D,I};
    
    enum MatchType curMat = S;
    int i = 0;
    int j = 0;
    int m = (*matspt).rind;
    int n = (*matspt).cind;
    char link = '|';
    int q;
    int r;
    if(m > 1){
         q = -(matspt->Dpt[m][n]);
         r = -((matspt->Dpt[m-1][n])+q);
    }
    if(n > 1){
         q = -(matspt->Ipt[m][n]);
         r = -((matspt->Ipt[m][n-1])+q);
    }
    (*algopt).top = (char *)check_malloc((m+n)*sizeof(char));
    (*algopt).mid = (char *)check_malloc((m+n)*sizeof(char));
    (*algopt).bot = (char *)check_malloc((m+n)*sizeof(char));
    (*algopt).score = (*matspt).Spt[0][0]; 
    (*algopt).alen = 0;
    while(i <= m && j <= n){
            char a = (*sonept).seq[i+1];
            char b = (*stwopt).seq[j+1];
            int current = (*algopt).alen;
        if(curMat == S){
            if (i == m && j == n)
                break;
            if(j == n || (*matspt).Spt[i][j] == (*matspt).Dpt[i][j] ){
                curMat = D;
                continue;
            }
            if(i == m || (*matspt).Spt[i][j] == (*matspt).Ipt[i][j] ){
                curMat = I;
                continue;
            }
            algopt->top[current] = a; 
            algopt->bot[current] = b; 
            if(a == b){
                algopt->mid[current] = link; 
            }
            else{
                algopt->mid[current] = ' '; 
            }
            (*algopt).alen ++;

            i++;
            j++;
            continue;
        }
       if(curMat == D){
           //Append a, -

            algopt->top[current] = a; 
            algopt->mid[current] = '-'; 
            algopt->bot[current] = ' '; 
            (*algopt).alen ++;
           if(i == m-1 || (*matspt).Dpt[i][j] == (*matspt).Spt[i+1][j]-q-r ){
               curMat = S;
           }
           i++;
           continue;
        }
      if(curMat == I){
           //Append -, b
            algopt->top[current] = ' '; 
            algopt->mid[current] = '-'; 
            algopt->bot[current] = b; 
            (*algopt).alen ++;

           if(j == n-1 || (*matspt).Ipt[i][j] == (*matspt).Spt[i][j+1]-q-r){
               curMat = S;
           }
           j++;
           continue;
      }
    }

}

void outputmat(char kind, struct seqtp *sonept, struct seqtp *stwopt, int **Mpt){
    
    printf("The type of the Matrix is %c \n", kind);
    //Print the rows
    int m = (*sonept).slen;
    int n = (*stwopt).slen;
    int i, j;
    //Print the title
    printf("%s \n", (*stwopt).name);
    for(j = 0; j < n; j++){
        printf(" %c ", (*stwopt).seq[j]);
    }
    printf("\n");
    printf("Colomn");
    for(j = 0; j < n; j ++){
        printf("  %d  ", j);
    }

    printf("\n %s", (*sonept).name);
    for(i = 0; i < m; i++){
        printf(" %c ", (*sonept).seq[i]);
        printf("Row %d  ", i );
        for(j=0;j < n; j++){
            printf(" %d " , Mpt[i][j]);
        }
        printf("\n");
    }

}
void outputalg(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct algtp *algopt){
    int row = ((*algopt).alen)/70;
    int i = 0;
    int currentPos = 1;
    int count_top = 1;
    int count_bot = 1;
    printf("%10s%s\n", "Sequence A: ",sonept->name);
    printf("%10s%d\n", "Length: ", sonept->slen);
    printf("\n");

    printf("%10s%s", "Sequence B: ", stwopt->name);
    printf("%10s%d", "Length: ", stwopt->slen);
    printf("\n");

    printf("%15s%d \n ","Alignment Score: ", algopt->score);
    printf("%15s%d \n","Length: ", algopt->alen);
    printf("\n");
    while(i <= row){

        printf("%9d \n", i*70+1);
       //Print 10 spaces
        printf("%9d ", count_top);
        //printf("%10s", "");
        //Print 70 charaters
        int j;
        int currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).top[i*70+j]);
            if((*algopt).top[i*70+j] != ' ')
                count_top++;
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                j = 71;
                break;
            }
        }

        printf("\n");
        currentChar = 0;
        printf("%10s", "");
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).mid[i*70+j]);
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                j = 71;
                break;
            }
        }
        printf("\n");
        printf("%9d ", count_bot);
        currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).bot[i*70+j]);
            if((*algopt).bot[i*70+j] != ' ')
                count_bot++;
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                i = row+1;
                j = 71;
                break;
            }
        }
        i++;
        printf("\n");
    }

}

void **get2dspace(int rind, int cind){
     void  **a2pt;
     int  j;
     if ( rind < 0 || cind < 0 )
             fatal("get2darr: negative parameter\n");
     a2pt = check_malloc((rind+1) * sizeof(int * ));
     for ( j = 0; j <= rind;j++ ) 
         a2pt[j] = (int *) check_malloc( (cind+1) * sizeof(int) );
     return a2pt;
}

void free2dspace(int rind, int **arr){
     if ( rind < 0 )
             fatal("get2darr: negative parameter\n");
     int i;
     for(i = 0; i <= rind; i++){
         free(arr[i]);
         arr[i] = NULL;
     }
     free(arr);
}


void getseq(const char *fname, struct seqtp *seqrec){

   FILE *fp;
   int len;
   char sequence[MaxLineLen];
   int ch;
   int j;
   int lcount;
   long temp;
   len = 0;
   fp = ckopen(fname,"r");
   while((ch = fgetc(fp)) != '\n')
       len++;
   if(len<=1)
       fatal("The name is not valid! \n");
   rewind(fp);
   //Read the first line to the name
   //seqrec->name = (char *)malloc((len)*sizeof(char));
   seqrec->name = (char *)malloc((len+1)*sizeof(char));//\0 char..overflow? and the \n char
   //Skip the >
   fseek(fp,1,SEEK_SET);
   fgets(seqrec->name, MaxLineLen, fp);
   //fscanf(fp, "%s", seqrec->name);
   seqrec->name[len-1] = '\0';
   //char * npt = seqrec->name;
   //while(*npt++ != '\0'){
   //    if(*npt == '\n')
   //        printf("Name contains newline char \n");
   //}
   //free(npt);


   temp = ftell(fp);
   //Read the lines
   lcount=0;
   len = 0;
   while(!feof(fp)){
       ch = fgetc(fp);
       if(ch == '\n'){
           lcount++;
       }
       else{
           len++;
       }
   }

   
   fseek(fp,temp,SEEK_SET);
   seqrec->seq  = NULL;
   seqrec->seq = (char *)check_malloc((len+2)*sizeof(char));
   seqrec->seq[0] = ' '; 
   seqrec->seq[1] = '\0';
   //Concatenate the sequence and a blank space
   for(j = 0; j < lcount; j++){
       //fgets(sequence, len, fp);
       fscanf(fp,"%s",sequence);
       strcat(seqrec->seq, sequence);
   }

    j = 0;
    while(seqrec->seq[j++] != '\0');
    seqrec->slen = j-2;
    fclose(fp);
}
void fatal(const char *msg)
{
     fprintf(stderr, "Error message: %s\n", msg);
     printf("Error message: %s\n", msg);
     exit(1); // stops unexpectedly.
}
void *check_malloc(size_t amount)
{
  void *tpt;
  /* Allocates a memory block in amount bytes. */
  tpt = malloc( amount );

  /* Checks if it was successful. */
  if ( tpt == NULL )
   { // prints a message to standard error device (console).
     fprintf(stderr, "No memory of %lu bytes\n", amount);
     printf("No memory of %lu bytes\n", amount);
     exit(1); // stops unexpectedly.
   }

  return tpt;
}
FILE *ckopen(const char *name, const char *mode)
{
	FILE *fopen(), *fp;

	if ((fp = fopen(name, mode)) == NULL)
	{ fprintf(stderr, "Cannot open %s\n", name);
	  printf("Cannot open %s\n", name);
	}
	return(fp);
}
