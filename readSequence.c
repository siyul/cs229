/*
 * =====================================================================================
 *
 *       Filename:  readSequence.c
 *
 *    Description:  Read sequence from a text file and print it to the stdout
 *
 *        Version:  1.2
 *        Created:  09/22/2014 05:29:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin  
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit().
#include <string.h> // A library with strcpy(), strcmp(), strlen().

struct seqtp   // a structure for three features of a sequence
 { char *name; // pointer to a string for keeping the name of a sequence
   char *seq;  // pointer to a char array for storing the sequence
   int  slen;  // the length of the sequence = char array length - 1,
 };            // where seq[1], seq[2], ... seq[n] are used
FILE *ckopen(const char *name, const char *mode);
void removeSpace(char *str);
#define MaxLineLen 1000

main(int argc, char *argv[]){
    FILE *fp;
    int len = 0;
    char sequence[MaxLineLen];
    int ch;
    int j;
    int lcount;
    long temp;
    struct seqtp *seqApt;
    fp = ckopen(argv[1],"r");
    //sequence = (char *)malloc((MaxLineLen)*sizeof(char));
    seqApt =(struct seqtp*)malloc(sizeof(struct seqtp));    
    //Read the first line to the name

    while((ch = fgetc(fp)) != '\n')
        len++;
    if(len <= 1)
        printf("Name not valid! \n");
    rewind(fp);
    seqApt->name = (char *)malloc((len+1)*sizeof(char));
    //Skip the >
    fseek(fp,1,SEEK_SET);
    fgets(seqApt->name, MaxLineLen, fp);
   seqApt->name[len-1] = '\0';
   char * npt = seqApt->name;
   while(*npt++ != '\0'){
       if(*npt == '\n')
           printf("Name contains newline char \n");
   }
    printf("The name is %s \n", seqApt->name);

   temp = ftell(fp);
   //printf("The current position is %d\n",temp);
   //Read the lines
   lcount=0;
   len = 0;
   while(!feof(fp)){
       ch = fgetc(fp);
       if(ch == '\n'){
           lcount++;
       }
       else{
           len++;
           //printf("%d Get one char\n", len);
       }
   }
   printf("%d lines counted\n",lcount);
   printf("%d char counted\n",len);

   fseek(fp,temp,SEEK_SET);
   //sequence[0] = ' ';
   seqApt->seq = (char *)malloc((len+3)*sizeof(char));
   seqApt->seq[0] = ' '; 
   printf("Before concatenation: %s\n",seqApt->seq); 

   //Concatenate the sequence and a blank space
   for(j = 0; j < lcount; j++){
        fscanf(fp,"%s",sequence);
        //fgets(sequence, sequenceLen, fp);
        printf("Sequence is %s\n",sequence);
        strcat(seqApt->seq, sequence);
   }

   //j = 0;
   //printf("The last sequence is \n");
   //while(sequence[j++] != '\0')
   //    printf("%d %c \n", j, sequence[j]);
    
    j = 0;
    while(seqApt->seq[j++] != '\0')
        printf("%d %c \n", j, seqApt->seq[j]);
    printf("The sequence is %s \n", seqApt->seq);
    seqApt->slen = j-2;
    printf("The length is %d \n", seqApt->slen );
    //free(sequence);
    fclose(fp);
    return 0;
}

FILE *ckopen(const char *name, const char *mode)
{
	FILE *fopen(), *fp;

	if ((fp = fopen(name, mode)) == NULL)
	{ fprintf(stderr, "Cannot open %s\n", name);
	  printf("Cannot open %s\n", name);
	}
	return(fp);
}
