/*
 * =====================================================================================
 *
 *       Filename:  proj2_main.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/08/2014 04:11:33 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include <iostream> // provides objects like cin and cout for sending data
                    // to and from the standard streams input and output.
		    // These objects are part of the std namespace.
#include <cstdlib>  // has exit etc.
#include <fstream>  // file streams and operations
#include <sstream>  // string streams and operations
using namespace std; // a container for a set of identifiers.
                     // Because of this line, there is no need to use std::cout
#include <typeinfo> // This header file is included for using typeid.
#include <stdexcept>

#include <stdio.h>
#include <string.h>
#include <ctype.h>  // A library with functions for testing characters.
#include "proj2.part1.h"
int main(int arc, char *argv[]){
    char * seqA_file = argv[1];
    char * seqB_file = argv[2];
    Direct short_one(seqA_file);
    //Direct long_one("sample.seq1.txt");
    Direct short_two(seqB_file);
    //Direct long_two("sample.seq2.txt");
    //printf("Sequence A is %s", short_one.getSeq());
    //printf("Sequence B is %s", short_two.getSeq());
    scoretp parampt;

    parampt.match = 10;
    parampt.mismat = -20;
    parampt.gopen = 40;
    parampt.gext = 2;

    //printf("Match score is %d, mismatch score is %d, open score is %d, extension score is %d \n", parampt.match, parampt.mismat,parampt.gopen, parampt.gext);
    //Direct &long_one_ref = long_one;
    //printf("Sequence A is %s", short_one_ref.getSeq());
    //Direct &long_two_ref = long_two;
    scoretp &param = parampt;
    //printf("Match score is %d, mismatch score is %d, open score is %d, extension score is %d \n", param.match, param.mismat,param.gopen, param.gext);
    Matrix mat(short_one, short_two, parampt);
    //Matrix mat_long (long_one, long_two, parampt);
    Alignment align(mat, param);
    //Alignment align_long(mat_long, param);
    //printf("Score is %d \n", align.getScore());
    //printf("Edit number is %d \n", align.getEditNum());
    //printf("Sub Insert Len is %d \n", align.getSubInsertLen());
    //printf("Alignment Len is %d \n", align.getAlignLen());
    Alignment &align_ref = align;
    //Alignment &align_long_ref = align_long;
    Encoded encode_align(align_ref);
    //Encoded encode_align_long(align_long_ref);
}
// Prints out an error message and stops.
void fatal(const char *msg)
{
     cerr << "Error message: " << string(msg) << endl;
     exit(1); // stops unexpectedly.
}

// Prints out an error message and stops.
void fatal(const char *msg, const char *name) 
{
     cerr << "Error message: " << string(msg) << string(name) << endl;
     exit(1); // stops unexpectedly.
}

// Opens an input file stream and checks for success.
void ckopeninf(ifstream &infile, const char *fname)
{
	infile.open(fname);
	if ( infile.fail() )
	  fatal("ckopeninf: cannot open input file ", fname);
}
int** get2dspace(int rowind, int colind){
     int  **a2pt;
     int  j;
     //if ( rind < 0 || cind < 0 )
     //        fatal("get2darr: negative parameter\n");
     a2pt = new int*[rowind+1];
     for ( j = 0; j <= rowind; j++ ) 
         //a2pt[j] = (int *) check_malloc( (cind+1) * sizeof(int) );
         a2pt[j] = new int[colind+1];
     return a2pt;
}
