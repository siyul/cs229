/*
 * =====================================================================================
 *
 *       Filename:  project2_unittest.cc
 *
 *    Description:  Google unit test for project 2 
 *
 *        Version:  1.0
 *        Created:  11/03/2014 03:19:53 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Siyu Lin
 *   Organization:  Iowa State University
 *
 * =====================================================================================
 */
#include <iostream> // provides objects like cin and cout for sending data
                    // to and from the standard streams input and output.
		    // These objects are part of the std namespace.
#include <cstdlib>  // has exit etc.
#include <fstream>  // file streams and operations
#include <sstream>  // string streams and operations
using namespace std; // a container for a set of identifiers.
                     // Because of this line, there is no need to use std::cout
#include <typeinfo> // This header file is included for using typeid.
#include <stdexcept>

#include <stdio.h>
#include <string.h>
#include <ctype.h>  // A library with functions for testing characters.
#include <limits.h>
#include "proj2.part1.h"
//#include "gtest/gtest.h"

sequence::testing::Test{

}
TEST(GetSequenceTest,shortSequenceName){
    char seq[] = " AGTAACGACCT";
    string name = "C";
    string &nref = name;
    Direct *testD = new Direct(nref,11,seq);
    EXPECT_EQ("C",testD->getName());
    char * dSeq = testD->getSeq();
    string testSeq(dSeq);
    EXPECT_EQ(" AGTAACGACCT",testSeq);
    EXPECT_EQ(11,testD->getLength());
    testD->setName("A");
    EXPECT_EQ("A",testD->getName());
    delete testD;
}
TEST(NormalConstructorTest,shortSequenceName){
    Direct *testD = new Direct("A1.txt");
    EXPECT_EQ("A",testD->getName());
    int length = testD->getLength();
    char * seqstr = new char[length+2];
    strcpy(seqstr, testD->getSeq());
    EXPECT_EQ(11,testD->getLength());
    EXPECT_EQ(' ', seqstr[0]);
    string testSeq(testD->getSeq());
    EXPECT_EQ(" AGTAACGACCT",testSeq);
    EXPECT_EQ('A', seqstr[1]);
    EXPECT_EQ('\0', seqstr[length+1]);
}
TEST(NormalConstructorTest,shortSequenceName){
    Direct *testD = new Direct("B1.txt");
    EXPECT_EQ("B",testD->getName());
    int length = testD->getLength();
    char * seqstr = new char[length+2];
    strcpy(seqstr, testD->getSeq());
    EXPECT_EQ(10,testD->getLength());
    EXPECT_EQ(' ', seqstr[0]);
    string testSeq(testD->getSeq());
    EXPECT_EQ(" AGCTATCGCT",testSeq);
    EXPECT_EQ('A', seqstr[1]);
    EXPECT_EQ('\0', seqstr[length+1]);
}
TEST(NormalConstructorTest,longSequenceName){
    Direct *testD = new Direct("sample.seq1.txt");
    EXPECT_EQ("Human",testD->getName());
    EXPECT_EQ(953,testD->getLength());
    char * seqstr = new char[testD->getLength()+2];
    int length = testD->getLength();
    strcpy(seqstr, testD->getSeq());
    EXPECT_EQ(' ', seqstr[0]);
    EXPECT_EQ('\0', seqstr[length+1]);
    EXPECT_EQ('C', seqstr[length]);
}
TEST(ProduceMatrix, shortSequence){
    
}
