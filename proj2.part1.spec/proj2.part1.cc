// Template for Part 1 of Project 2
// @author 
#include "proj2.part1.h"
#include <string.h>

Direct::Direct(){
    seq = NULL;
    length = 0;
    name = "";
}
Direct::Direct(const char *fname) {
    std::fstream infile;
    std::string line;
    char symbol;
    this->length = 0;

    infile.open(fname);
    if(infile.fail()){
        std::cout<<"Can not open file" << endl;
    }
    infile.seekg(1,infile.beg);    
    std::getline(infile,line);
    this->name = line;
    long begin = infile.tellg();
    //std::cout<<"The location is " << infile.tellg()<< endl;

    //while(infile>>std::skipws>>symbol){
    // //   std::cout<<"Length increased by 1" << endl;
    //    (this->length)++;
    //}

    infile.seekg(0, infile.end);
    long end = infile.tellg();
    infile.seekg(begin, infile.beg);
    while(infile.tellg() != end && infile.get(symbol)){
        if(symbol != '\n' && symbol != '\0' && symbol != ' '){
            (this->length)++;
        }
    }
    //std::cout<<"The location is " << infile.tellg()<< endl;
    seq = new char[(this->length)+2]();
    seq[0] = ' ';
    seq[(this->length)+1] = '\0';
    infile.seekg(begin, infile.beg);
    ///std::cout<<"The location is " << infile.tellg()<< endl;
    //std::cout<<"The length is " << this->length<< endl;

    int i;
    for(i = 1; infile.get(symbol); ){
        if(symbol != '\n' && symbol != '\0' && symbol != ' '){
            seq[i++] = symbol;
            //if(i == this->length)
            //    std::cout<<"Read a char "<< symbol<< endl;
        }
    }
}// normal constructor

Direct::Direct(const Direct &obj){
    name = obj.name;
    length = obj.length;
    seq = new char[length+2];
    seq = obj.seq;
} // copy constructor

Direct& Alignment::getOrigin() const {
    return origin;
}// returns origin.
Direct& Alignment::getDerived() const {
    return derived;
}// returns origin.
Direct::~Direct(){
    delete[] seq;
}
Direct& Matrix::getOrigin() const{return origin;};  // returns origin.
Direct& Matrix::getDerived() const{return derived;}; // returns derived.
int Matrix::getRowInd() const{return rowind;};      // returns rowind.
int Matrix::getColInd() const{return colind;};      // returns rowind.
int** Matrix::getMat(char kind) const{
    if (kind == 'D'){
        return Dmat;
    }
    else if (kind == 'I'){
        return Imat;
    }
    else if (kind == 'S'){
        return Smat;
    }
    else{
       return Smat; 
    }
}


Matrix::Matrix(Direct &seqone, Direct &seqtwo, struct scoretp &param): origin(seqone), derived(seqtwo){ 
        rowind = seqone.getLength();
        colind = seqtwo.getLength();
        scoretp &paramt = param;
        computemats(origin,derived,paramt);
    }; // normal constructor
int** Matrix::get2dspace(int rowind, int colind){
     int  j;
     //if ( rind < 0 || cind < 0 )
     //        fatal("get2darr: negative parameter\n");
     int** a2pt = new int*[rowind+1];
     for ( j = 0; j <= rowind; j++ ) {
         a2pt[j] = new int[colind+1];
     }

     return a2pt;
}
void Matrix::free2dspace(int rowind, int **arr){
     //if ( rind < 0 )
     //        fatal("get2darr: negative parameter\n");
     int i;
     for(i = 0; i <= rowind ; i++){
         delete[] arr[i];
     }
     delete[] arr;
}

void Matrix::computemats(Direct &seqone, Direct &seqtwo, struct scoretp &param){
    int i;
    int j;
    int rind = rowind;
    int cind = colind;
    int gopen = -param.gopen;
    int gext = -param.gext;
    char * seqone_seq;
    char * seqtwo_seq;
    
    Dmat = get2dspace(rind,cind);
    Imat = get2dspace(rind,cind);
    Smat = get2dspace(rind,cind);
    seqone_seq = new char[rind+2];
    seqtwo_seq = new char[cind+2];
    strcpy(seqone_seq,seqone.getSeq());
    strcpy(seqtwo_seq,seqtwo.getSeq());
    
    this->Smat[rind][cind] = 0;
    this->Dmat[rind][cind] = this->Smat[rind][cind]-gopen;
    this->Imat[rind][cind] = this->Smat[rind][cind]-gopen;
    for(i=rind-1; i >=0; i--)
    {
       Dmat[i][cind] = Dmat[i+1][cind]-gext;
       Smat[i][cind] = Dmat[i][cind];
       Imat[i][cind] = Smat[i][cind]-gopen;
    }

    for(j=cind-1;j>=0; j--){
       Imat[rind][j] = Imat[rind][j+1]-gext;
       Smat[rind][j] = Imat[rind][j];
       Dmat[rind][j] = Smat[rind][j]-gopen;
    }

    for(i = rind-1; i >= 0; i --){

       Dmat[i][cind] = Dmat[i+1][cind]-gext;
       Smat[i][cind] = Dmat[i][cind];
       Imat[i][cind] = Smat[i][cind]-gopen;
        for(j = cind-1; j >= 0; j --){
            Dmat[i][j] = max((Dmat[i+1][j]-gext),(Smat[i+1][j]-gext-gopen));
            Imat[i][j] = max((Imat[i][j+1]-gext),(Smat[i][j+1]-gext-gopen));

            int diMax = max((Dmat[i][j]),(Imat[i][j]));
            char a = seqone_seq[i+1];
            char b = seqtwo_seq[j+1];
            int matchingS = matchingScore(a, b, param);
            int s = (Smat[i+1][j+1])+matchingS;
            Smat[i][j] = max((s),(diMax));
        }
    }
    delete[] seqone_seq;
    delete[] seqtwo_seq;
}

string Matrix::toString(char kind) const{
    //std::vector<char> mat_str; 
    //unsigned char * mat_str = new unsigned char[];
    char buffer[1000];
    stringstream outstr;
    int i,j;
    int m = getRowInd();
    int n = getColInd();
    int** Mat = getMat(kind);
    for(i = 0; i < m; i++){
        for (j = 0; j < n ; j ++){
            int n = sprintf(buffer, "%d ", Mat[i][j]);
            outstr << buffer;
        }
        sprintf(buffer, "\n");
        outstr << buffer;
    }
    return outstr.str();
}
Matrix::~Matrix(){
    free2dspace(rowind,Dmat);
    free2dspace(rowind,Imat);
    free2dspace(rowind,Smat);
}

Alignment::Alignment(Matrix &matobj, struct scoretp &param): origin(matobj.getOrigin()), derived(matobj.getDerived()){
        int **Smat = matobj.getMat('S');
        score = Smat[0][0];
        subinsertlen = 0;
        editnum = 0;
        alignlen = 0;
        producealg(origin, derived, matobj, param);
}; // normal constructor
void Alignment::producealg(Direct &seqone, Direct &seqtwo, Matrix &matobj, struct scoretp &param){
    enum MatchType {S,D,I};
    
    enum MatchType curMat = S;
    int i = 0;
    int j = 0;
    int intlen = 0;
    int m = matobj.getRowInd();
    int n = matobj.getColInd();
    char link = '|';
    int q = -param.gopen;
    int r = -param.gext;
    int ** Spt = matobj.getMat('S');
    int ** Dpt = matobj.getMat('D');
    int ** Ipt = matobj.getMat('I');
    char *seqone_seq;
    char *seqtwo_seq;

    seqone_seq = new char[m+2];
    seqtwo_seq = new char[n+2];
    strcpy(seqone_seq,origin.getSeq());
    strcpy(seqtwo_seq,derived.getSeq());
    //printf("Sequence A is %s \n", origin.getSeq());
    //printf("Sequence B is %s \n", derived.getSeq());
    top = new char[m+n];
    mid = new char[m+n];
    bot = new char[m+n];
    score = Spt[0][0];
    while(i <= m && j <= n){
            char a = seqone_seq[i+1];
            char b = seqtwo_seq[j+1];
            int current = alignlen;
        if(curMat == S){
            if (i == m && j == n)
                break;
            if(j == n || Spt[i][j] == Dpt[i][j] ){
                curMat = D;
                editnum++;
                continue;
            }
            if(i == m || Spt[i][j] == Ipt[i][j] ){
                curMat = I;
                editnum++;
                intlen++;
                continue;
            }
            top[current] = a; 
            bot[current] = b; 
            if(a == b){
                mid[current] = link; 
            }
            else{
                editnum++;
                subinsertlen++;
                mid[current] = ' '; 
            }
            alignlen ++;

            i++;
            j++;
            continue;
        }
       if(curMat == D){
           //Append a, -

            top[current] = a; 
            mid[current] = '-'; 
            bot[current] = ' '; 
            alignlen ++;
           if(i == m-1 || Dpt[i][j] == Spt[i+1][j]-q-r ){
               curMat = S;
           }
           i++;
           continue;
        }
      if(curMat == I){
           //Append -, b
            top[current] = ' '; 
            mid[current] = '-'; 
            bot[current] = b; 
            alignlen ++;
            intlen++;

           if(j == n-1 || Ipt[i][j] == Spt[i][j+1]-q-r){
               curMat = S;
               subinsertlen += intlen;
           }
           j++;
           continue;
      }
    }
    top[alignlen] = '\0';
    mid[alignlen] = '\0';
    bot[alignlen] = '\0';
    toString();
    delete[] seqone_seq;
    delete[] seqtwo_seq;


}
int Alignment::getScore() const {return score;}; // returns score.
int Alignment::getEditNum() const {return editnum;}; // returns editnum.
int Alignment::getSubInsertLen() const {return subinsertlen;}; // returns insertlen.
int Alignment::getAlignLen() const{return alignlen;}; // returns alignlen.
string Alignment::toString() const{
    char buffer[1000];
    stringstream outstr;
    int row = alignlen/70;
    int i = 0;
    int currentPos = 1;
    int count_top = 1;
    int count_bot = 1;

    sprintf(buffer, "%10s ", "Sequence A: ");
    outstr << buffer;
    string o_name = origin.getName();
    outstr << o_name;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"%10s%d\n", "Length: ", origin.getLength());
    outstr << buffer;
    sprintf(buffer, "\n");
    outstr << buffer;

    sprintf(buffer, "%10s", "Sequence B: ");
    outstr << buffer;
    string d_name = derived.getName();
    outstr << d_name;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"%10s%d\n", "Length: ", derived.getLength());
    outstr << buffer;
    sprintf(buffer, "\n");
    outstr << buffer;

    sprintf(buffer, "%15s%d \n ","Alignment Score: ", score);
    outstr << buffer;
    sprintf(buffer, "%15s%d \n","Length: ", alignlen);
    outstr << buffer;
    sprintf(buffer,"\n");
    outstr << buffer;

    while(i <= row){

        sprintf(buffer,"%9d \n", i*70+1);
       //Print 10 spaces
        sprintf(buffer,"%9d ", count_top);
        //printf("%10s", "");
        //Print 70 charaters
        outstr << buffer;
        int j;
        int currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", top[i*70+j]);
            outstr << buffer;
            if(top[i*70+j] != ' ')
                count_top++;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                j = 71;
                break;
            }
        }

        sprintf(buffer,"\n");
        outstr << buffer;
        currentChar = 0;
        sprintf(buffer,"%10s", "");
        outstr << buffer;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", mid[i*70+j]);
            outstr << buffer;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                j = 71;
                break;
            }
        }
        sprintf(buffer,"\n");
        outstr << buffer;
        sprintf(buffer,"%9d ", count_bot);
        outstr << buffer;
        currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", bot[i*70+j]);
            outstr << buffer;
            if(bot[i*70+j] != ' ')
                count_bot++;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                i = row+1;
                j = 71;
                break;
            }
        }
        i++;
        sprintf(buffer,"\n");
        outstr<<buffer;
    }
    //outstr << buffer;
    cout<<outstr.str()<<endl;
    return outstr.str();
}

Alignment::~Alignment(){
    delete[] top;
    delete[] mid;
    delete[] bot;
}

char* Alignment::getRow(char kind) const{
    if(kind == 't'){
        return top;
    }
    else if (kind == 'm'){
        return mid;
    }
    else if (kind == 'b'){
        return bot;
    }
    else{
        return NULL;
    }

}

Encoded::Encoded(Alignment &obj): origin(obj.getOrigin()){
    editnum = obj.getEditNum();
    subinsertlen = obj. getSubInsertLen();
    dname = origin.getName();
    operation = new Edit[editnum];
    subinsertion = new char[subinsertlen];
    encodeOrigin(obj);
    decodeOrigin(origin);
}
void Encoded::encodeOrigin(Alignment &obj){
    int i =  1; 
    int j = 1;
    int opind = 0;
    int snind = 0;
    int ind = 0;
    int alength = obj.getAlignLen();
    char * top = obj.getRow('t');
    char * mid = obj.getRow('m');
    char * bot = obj.getRow('b');
    char link = '|';
    char blank = ' ';
    for(ind = 0; ind < alength;){
        if(top[ind] == bot [ind] && mid[ind] == link ){
            i++;
            j++;
            ind++;
            continue;
        }
        else if ( top[ind] != bot[ind] && mid[ind] == blank ) {
            operation[opind].position = i;
            operation[opind].indel = 0;
            subinsertion[snind++] = bot[ind];
            i++;
            j++;
            ind++;
            opind++;
            continue;
        }
        else{
            int gaplen = 0;
            while(mid[ind+gaplen] == '-'){
                gaplen++;
            }
            operation[opind].position = i;
            if( top[ind] != blank && mid[ind] == '-'){
                i+= gaplen;
                operation[opind++].indel = -gaplen;
                ind += gaplen;
                //continue;
            } //Deletion gap
            else{
                j += gaplen;
                operation[opind++].indel = gaplen;
                for(; gaplen >= 1; gaplen--){
                    subinsertion[snind++] = bot[ind++];
                }
                //continue;
            }//Insetion Gap
        }
    }
    dlength  = j-1;
}
void Encoded::decodeOrigin(Direct &origin){
    int i = 1;
    int j = 1;
    int snind = 0;
    int opind = 0;
    derived = new char[dlength+2];
    char *originA = origin.getSeq();
    int m = origin.getLength();
    derived[0] = ' ';
    for(; opind<editnum; opind++){
        int oppos = operation[opind].position;
        int indel = operation[opind].indel;
        while(i <= oppos){
            derived[j++] = originA[i++];
        }
        if(indel < 0){
            printf("Operation %d :: Position: %d Indel: %d Deletion \n", opind, oppos, indel);
            i -= indel;
        }
        else if (indel ==0){
            printf("Operation %d :: Position: %d Indel: %d Substitution: %c \n", opind, oppos, indel, subinsertion[snind]);
            i++;
            derived[j++] = subinsertion[snind++];
        }
        else{
            char * temp = new char[indel+1];
            int k = 0;
            for(; indel; indel--){
                temp[k++] = subinsertion[snind];
                derived[j++] = subinsertion[snind++]; 
            }
            temp[k] = '\0';
            printf("Operation %d :: Position: %d Indel: %d Insertion: %s \n", opind, oppos, indel, temp);
            delete[] temp;
        }
     }
    while(i <= m){
        derived[j++] = originA[i++];
    }
    derived[dlength+1] = '\0';
}
int Encoded::getEditNum() const{
    return editnum;
}
struct Edit* Encoded::getOperation() const{
    return operation;
}
int Encoded::getSubInsertLen() const{
    return subinsertlen;
}
char* Encoded::getSubInsertion() const{
    return subinsertion;
}
int Encoded::getDLength() const{
    return dlength;
}
string Encoded::getDName() const{
    return dname;
}
Direct& Encoded::getOrigin() const {
    return origin;
}
char* Encoded::getDSeq() const {
    return derived;
}// deirves a sequence and turns it in a char array.
Encoded::~Encoded(){
    delete[] subinsertion;
    delete[] operation;
    delete[] derived;
}
