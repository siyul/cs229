//@Siyu Lin
//@author: Siyu Lin
#include "Direct.hh"
Direct::Direct(){
// Sets seq to NULL.
    seq = NULL;
    length = 0;
    name = "";
}
Direct::Direct(const char *fname) {
// Input parameter fname is a pointer to a string for storing the name of a DNA sequence file.
// A Direct object is constructed to save the name,
// length and DNA letters of the sequence in the file.
// A proper amount of memory needs to be allocated for the seq member.

// This constructor opens the file whose name is pointed to by fname,
// reads the name, length and DNA letters of the sequence in the file,
// dynamically allocates a proper amount of memory for the seq member
// and saves the data into the member variables.
// The constructor prints out a proper error message if fname is NULL,
// no file exists with the given name.
    std::fstream infile;
    std::string line;
    char symbol;
    this->length = 0;

    infile.open(fname);
    if(infile.fail()){
        std::cout<<"Can not open file" << endl;
    }
    infile.seekg(1,infile.beg);    
    std::getline(infile,line);
    this->name = line;
    long begin = infile.tellg();
    //std::cout<<"The location is " << infile.tellg()<< endl;

    //while(infile>>std::skipws>>symbol){
    // //   std::cout<<"Length increased by 1" << endl;
    //    (this->length)++;
    //}

    infile.seekg(0, infile.end);
    long end = infile.tellg();
    infile.seekg(begin, infile.beg);
    while(infile.tellg() != end && infile.get(symbol)){
        if(symbol != '\n' && symbol != '\0' && symbol != ' '){
            (this->length)++;
        }
    }
    //std::cout<<"The location is " << infile.tellg()<< endl;
    seq = new char[(this->length)+2]();
    seq[0] = ' ';
    seq[(this->length)+1] = '\0';
    infile.seekg(begin, infile.beg);
    ///std::cout<<"The location is " << infile.tellg()<< endl;
    //std::cout<<"The length is " << this->length<< endl;

    int i;
    for(i = 1; infile.get(symbol); ){
        if(symbol != '\n' && symbol != '\0' && symbol != ' '){
            seq[i++] = symbol;
            //if(i == this->length)
            //    std::cout<<"Read a char "<< symbol<< endl;
        }
    }
}// normal constructor

Direct:: Direct(string &stag, int slen, char *sarr)
// Input parameter stag is a reference to a string object as the name of a sequence.
// Input parameter slen is the length of the sequence.
// Input parameter sarr is a pointer to a char array with the sequence
// saved at index 1 through index slen.

// If slen is negative or sarr is NULL, prints out an error message with fatal. 
// A copy of the string object is saved in the member name.
// The value slen is saved in the member length.
// Memory is allocated to the member seq and a copy of the sequence is saved in seq.
{
    if(slen == 0 || sarr == NULL){
        fatal("The length is negative or sequence is not valid \n");
    }
    name = stag;
    length = slen;
    seq = new char[slen+2];
    strcpy(seq,sarr);

}
Direct::Direct(const Direct &obj){
// A deep copy is made.
    name = obj.name;
    length = obj.length;
    seq = new char[length+2];
    for(int i=0; i < length+2; i++)
        seq[i] = obj.seq[i];
    //seq = obj.seq; //Shallow copy!
} // copy constructor

Direct::~Direct(){
    if(seq != NULL){
        delete[] seq;
    }
}

string Direct:: getName() const
// Returns the member name.
{
    return name;
}

int Direct:: getLength() const
// Returns the member length.
{
    return length;
}

char* Direct:: getSeq() const
// Returns the member seq.
{
    return seq;
}
