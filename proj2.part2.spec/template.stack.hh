//@author: Siyu Lin
#ifndef TEMPLATE_STACK_H
#define TEMPLATE_STACK_H
template<class T>
class Stack{
        struct Node{
            T *element;
            struct Node *next;
            //Node(T *elem){element = elem;}
        } *top;
        int count;
        //Stack minStack;
    public:
        Stack();
        ~Stack();
        void push(T *obj);
        T *pop();
        T *peek();
        bool isEmpty();
        int getCount() const{ return count;};

};

template<class T>
Stack<T>::Stack(){
    top = NULL;
    count = 0;
    //printf("Count is %d\n", count);
    //minStack = Stack();
}
template<class T>
Stack<T>::~Stack(){
    Node *tmp, *u;
    for(tmp = top;tmp; tmp = u){
        u = tmp->next;
        //delete tmp->element;
        delete tmp;
    }
}
template<class T>
void Stack<T>::push(T *obj){
    Node *cur = new Node;
    cur->element = obj;
    cur->next = top;
    top = cur;
    count ++;
    //printf("Count is %d\n", count);
    //if (minStack.isEmpty() || *(cur->element) <= *(minStack.peek())){
    //    minStack.push(obj);
    //}
}
template<class T>
T* Stack<T>::pop(){
    if(!isEmpty()){
        T *element;
        Node * temp;
        temp = top;
        top = temp->next;
        count--;
        element = temp->element;
        delete temp;
        return element;
    }
    else{
        return NULL;
    }
}
template<class T>
T* Stack<T>::peek(){
    if(top != NULL){
        T * element = top->element;
        return element;
    }
    else{
        return NULL;
    }
}
template<class T>
bool Stack<T>::isEmpty(){
    return (count==0)?true:false;
}
template<class T>
T* findMin(Stack<T> &stack){
    Stack<T> *tmp = new Stack<T>();
    if(!stack.isEmpty()){
        T *cur = stack.pop();  
        tmp->push(cur);
        T *min = cur;
        //T *element;
        //for(cur = stack.pop(); !stack.isEmpty();cur = stack.pop()){
        //    //T *element = stack.pop();
        //    tmp->push(cur);
        //    if(*cur <= *min){
        //        min = cur;
        //    }
        //}
        while(!stack.isEmpty()){
            //T *element = stack.pop();
            cur = stack.pop();
            tmp->push(cur);
            if(*cur <= *min){
                min = cur;
            }
        }
        //Restore the stack
        while(!tmp->isEmpty()){
            stack.push(tmp->pop());
        }
        delete tmp;
        return min;
    }
    return 0;
}
template<class T>
T* findMax(Stack<T> &stack){
    Stack<T> *tmp = new Stack<T>();
    if(!stack.isEmpty()){
        T *cur = stack.pop();  
        tmp->push(cur);
        T *max = cur;
        //T *element;
        //for(cur = stack.pop(); !stack.isEmpty();cur = stack.pop()){
        //    //T *element = stack.pop();
        //    tmp->push(cur);
        //    if(*cur <= *min){
        //        min = cur;
        //    }
        //}
        while(!stack.isEmpty()){
            //T *element = stack.pop();
            cur = stack.pop();
            tmp->push(cur);
            if(*max <= *cur){
                max = cur;
            }
        }
        //Restore the stack
        while(!tmp->isEmpty()){
            stack.push(tmp->pop());
        }
        delete tmp;
        return max;
    }
    return 0;
}
#endif
