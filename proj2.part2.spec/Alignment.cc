//@author: Siyu Lin
#include "Alignment.hh"
Alignment::Alignment(Matrix &matobj, struct scoretp &param): origin(matobj.getOrigin()), derived(matobj.getDerived()){
// Input parameter matobj is a reference to a Matrix object.
// Input parameter param is a reference to a struct scoretp variable with scoring parameters.

// The constructor initializes some member variables by calling public
// functions of Matrix from the object matobj.
// Then it allocates memory for each of the three char arrays,
// produces an optimal alignment by tracing through the matrices in the object matobj,
// and saves the alignment along with its score and length and editnum
// and subinsertlen in corresponding member variables.
// The alignment is represented by using three rows of characters (three char arrays).
// If the lengths of both sequences is zero, prints out an error message.

// Note that the length of an optimal alignment cannot exceed the sum of
// the lengths of the two DNA sequences.
        int **Smat = matobj.getMat('S');
        score = Smat[0][0];
        subinsertlen = 0;
        editnum = 0;
        alignlen = 0;
        if(origin.getLength()==0 && derived.getLength()==0){
            fatal("The lengths can not be both 0 \n");
        }
        producealg(origin, derived, matobj, param);
}; // normal constructor
void Alignment::producealg(Direct &seqone, Direct &seqtwo, Matrix &matobj, struct scoretp &param){
    enum MatchType {S,D,I};
    
    enum MatchType curMat = S;
    int i = 0;
    int j = 0;
    int intlen = 0;
    int m = matobj.getRowInd();
    int n = matobj.getColInd();
    char link = '|';
    int q = param.gopen;
    int r = param.gext;
    int ** Spt = matobj.getMat('S');
    int ** Dpt = matobj.getMat('D');
    int ** Ipt = matobj.getMat('I');
    char *seqone_seq;
    char *seqtwo_seq;

    seqone_seq = new char[m+2];
    seqtwo_seq = new char[n+2];
    strcpy(seqone_seq,origin.getSeq());
    strcpy(seqtwo_seq,derived.getSeq());
    //printf("Sequence A is %s \n", origin.getSeq());
    //printf("Sequence B is %s \n", derived.getSeq());
    top = new char[m+n];
    mid = new char[m+n];
    bot = new char[m+n];
    score = Spt[0][0];
    while(i <= m && j <= n){
            char a = seqone_seq[i+1];
            char b = seqtwo_seq[j+1];
            int current = alignlen;
        if(curMat == S){
            if (i == m && j == n)
                break;
            if(j == n || Spt[i][j] == Dpt[i][j] ){
                curMat = D;
                editnum++;
                continue;
            }
            if(i == m || Spt[i][j] == Ipt[i][j] ){
                curMat = I;
                editnum++;
                intlen = 0;
                continue;
            }
            top[current] = a; 
            bot[current] = b; 
            if(a == b){
                mid[current] = link; 
            }
            else{
                editnum++;
                subinsertlen++;
                mid[current] = ' '; 
            }
            alignlen ++;

            i++;
            j++;
            continue;
        }
       if(curMat == D){
           //Append a, -

            top[current] = a; 
            mid[current] = '-'; 
            bot[current] = ' '; 
            alignlen ++;
           if(i == m-1 || Dpt[i][j] == Spt[i+1][j]-q-r ){
               curMat = S;
           }
           i++;
           continue;
        }
      if(curMat == I){
           //Append -, b
            top[current] = ' '; 
            mid[current] = '-'; 
            bot[current] = b; 
            alignlen ++;
            intlen++;

           if(j == n-1 || Ipt[i][j] == Spt[i][j+1]-q-r){
               curMat = S;
               subinsertlen += intlen;
           }
           j++;
           continue;
      }
    }
    top[alignlen] = '\0';
    mid[alignlen] = '\0';
    bot[alignlen] = '\0';
    //toString();
    delete[] seqone_seq;
    delete[] seqtwo_seq;
    //printf("The sub insert len is %d \n", subinsertlen);
}
int Alignment::getScore() const {return score;}; // returns score.
int Alignment::getEditNum() const {return editnum;}; // returns editnum.
int Alignment::getSubInsertLen() const {return subinsertlen;}; // returns insertlen.
int Alignment::getAlignLen() const{return alignlen;}; // returns alignlen.
string Alignment::toString() const{
// Input parameter sonept is a pointer to a structure with the features of one DNA sequence.
// Input parameter stwopt is for another DNA sequence.
// Input parameter matspt is a pointer to a structure with three matrices.
// Input parameter algopt is a pointer to a structure for an optimal alignment.

// The function returns a summary of sequence and alignment information
// and the alignment in the form of string.
// The summary includes the name and length of each sequence,
// the score and length of the alignment. the number of edit operations,
// and the length of substitutions and insertions.
// The alignment is reported in sections of 70 characters, with each section consisting
// of three rows. The sequence positions of the first DNA letters in each section are
// reported in the left margin of 10 spaces.
    char buffer[1000];
    stringstream outstr;
    int row = alignlen/70;
    int i = 0;
    //int currentPos = 1;
    int count_top = 1;
    int count_bot = 1;

    sprintf(buffer, "%10s ", "Sequence A: ");
    outstr << buffer;
    string o_name = origin.getName();
    outstr << o_name;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"%10s%d\n", "Length: ", origin.getLength());
    outstr << buffer;
    sprintf(buffer, "\n");
    outstr << buffer;

    sprintf(buffer, "%10s", "Sequence B: ");
    outstr << buffer;
    string d_name = derived.getName();
    outstr << d_name;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"%10s%d\n", "Length: ", derived.getLength());
    outstr << buffer;
    sprintf(buffer, "\n");
    outstr << buffer;

    sprintf(buffer, "%20s%d \n ","Alignment Score: ", score);
    outstr << buffer;
    sprintf(buffer, "%20s%d \n","Length: ", alignlen);
    outstr << buffer;
    sprintf(buffer,"\n");
    outstr << buffer;

    sprintf(buffer, "%20s%d ","Number of edit Operations:", getEditNum());
    outstr << buffer;
    sprintf(buffer,"\n");
    outstr << buffer;
    
    sprintf(buffer, "%20s%d  ","Length of Substitutions and Insertions:", getSubInsertLen());
    outstr << buffer;
    sprintf(buffer,"\n");
    outstr << buffer;

    while(i <= row){

        sprintf(buffer,"%9d \n", i*70+1);
       //Print 10 spaces
        sprintf(buffer,"%9d ", count_top);
        //printf("%10s", "");
        //Print 70 charaters
        outstr << buffer;
        int j;
        int currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", top[i*70+j]);
            outstr << buffer;
            if(top[i*70+j] != ' ')
                count_top++;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                j = 71;
                break;
            }
        }

        sprintf(buffer,"\n");
        outstr << buffer;
        currentChar = 0;
        sprintf(buffer,"%10s", "");
        outstr << buffer;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", mid[i*70+j]);
            outstr << buffer;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                j = 71;
                break;
            }
        }
        sprintf(buffer,"\n");
        outstr << buffer;
        sprintf(buffer,"%9d ", count_bot);
        outstr << buffer;
        currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            sprintf(buffer,"%c", bot[i*70+j]);
            outstr << buffer;
            if(bot[i*70+j] != ' ')
                count_bot++;
            currentChar++;

            if(i == row && currentChar == (alignlen%70)){
                i = row+1;
                j = 71;
                break;
            }
        }
        i++;
        sprintf(buffer,"\n");
        outstr<<buffer;
    }
    //outstr << buffer;
    //cout<<outstr.str()<<endl;
    return outstr.str();
}

Alignment::~Alignment(){
    delete[] top;
    delete[] mid;
    delete[] bot;
}

char* Alignment::getRow(char kind) const{
// Returns top if kind is 'T'.
// Returns mid if kind is 'M'.
// Returns bot if kind is 'B'.
// Prints out an error if kind has no expeced value.
    if(kind == 'T'){
        return top;
    }
    else if (kind == 'M'){
        return mid;
    }
    else if (kind == 'B'){
        return bot;
    }
    else{
        fatal("The parameter is not valid, please specify it to 'T', 'M' or 'B' \n");
    }
    return 0;
}
Direct& Alignment::getOrigin() const {
    return origin;
}// returns origin.
Direct& Alignment::getDerived() const {
    return derived;
}// returns origin.

