//@author: Siyu Lin
#include "prototype.hh"
#include "wrapper.hh"
#include "Alignment.hh"
#include "Direct.hh"
#include "Encoded.hh"
#include "Matrix.hh"
#include "template.stack.hh"
int main(int argc, char *argv[])
{
   if ( argc <= 2 )
   { cerr << "Usage: " << argv[0] << " Seq0 seq_1 seq_2 ... " << endl;
     exit(1);
   }

   char * seqA_file = argv[1];
   char * seq_file;
   Direct seqone(seqA_file);
   Encoded  *encoded_cmp_pt;
   Encoded  *compressed_cmp_pt;
   scoretp pararec;
  
   pararec.mismat = -20;
   pararec.gopen = 40;
   pararec.gext = 2;
   pararec.match = 10; 

   //scoretp &param = pararec;
   Stack<Encoded> * encoded_stack = new Stack<Encoded>();
   Stack<Encoded> * compressed_stack = new Stack<Encoded>();
   for(int i = 2; i < argc; i++){
       seq_file = argv[i];
       Direct seq_cmp(seq_file);
       //cout << seq_cmp.getSeq() << endl;
       Matrix mat_cmp(seqone,seq_cmp, pararec);
       Alignment align_cmp(mat_cmp,pararec);
       encoded_cmp_pt = new Encoded(align_cmp);
       compressed_cmp_pt = new Compressed(align_cmp);
       //Encoded encoded(align_cmp);
       //cout << encoded_cmp_pt->getDSeq() << " " <<  "Number of difference is " << encoded_cmp_pt->getNumDiff() << endl;
       //encoded_cmp_pt = &encoded;
       encoded_stack->push(encoded_cmp_pt);
       compressed_stack->push(compressed_cmp_pt);
       //delete mat_cmp_pt;
       //delete align_cmp_pt;
   }
   Encoded * min_encoded = findMin(*encoded_stack);
   Encoded * max_encoded = findMax(*encoded_stack);
   cout << "A stack of encoded objects" << endl;
   cout << "Min encoded's number of diff: " << min_encoded->getNumDiff() << endl;
   cout << "Min encoded's Name: " << min_encoded->getDName() << endl;
   cout << "Min encoded: " << min_encoded->getDSeq() << endl;
   cout << endl;
   cout << "Max encoded's number of diff: " << max_encoded->getNumDiff() << endl;
   cout << "Max encoded's Name: " << max_encoded->getDName() << endl;
   cout << "Max encoded: " << max_encoded->getDSeq() << endl;
   cout << endl;

   Encoded * min_compressed = findMin(*compressed_stack);
   Encoded * max_compressed = findMax(*compressed_stack);
   cout << "A stack of compressed objects" << endl;
   cout << "Min compressed's number of diff: " << min_compressed->getNumDiff() << endl;
   cout << "Min compressed's Name: "<< min_compressed->getDName() << endl;
   cout << "Min compressed: " << min_compressed->getDSeq() << endl;
   cout << endl;
   cout << "Max compressed's number of diff: " << max_compressed->getNumDiff() << endl;
   cout << "Max compressed's Name: " << max_compressed->getDName() << endl;
   cout << "Max compressed: " << max_compressed->getDSeq() << endl;
   cout << endl;
  // Declares and initializes a struct scoretp variable named pararec.
  // Declares and constructs a Direct object named seqone for the sequence in file argv[1].

    //Stack<char>  * test_stack = new Stack<char>();
    //char* test_chars = new char[10];
    //for(int i = 0; i < 10; i++){
    //   test_chars[i] = 'a'+i;
    //   test_stack->push(&test_chars[i]);
    //}
    //cout << test_stack->getCount() << endl;
    //cout << *findMin(*test_stack)<< endl;
   for(int i = 2; i < argc; i++){
       Encoded * temp_encoded = encoded_stack->pop();
       delete temp_encoded;
       Encoded * temp_compressed = compressed_stack->pop();
       delete temp_compressed;
   }
   //delete test_stack;
   //delete[] test_chars;
   delete encoded_stack;
   delete compressed_stack;
  return 0;
}

