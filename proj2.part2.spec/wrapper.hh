//@author: Siyu Lin
#ifndef WRAPPER_H
#define WRAPPER_H
#include "prototype.hh"
void fatal(const char *msg); // a wrapper function
void fatal(const char *msg, const char *name); // a wrapper function
void ckopeninf(ifstream &infile, const char *fname); // a wrapper function
#endif
