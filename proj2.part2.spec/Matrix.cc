//@author: Siyu Lin
#include "Matrix.hh"
Matrix::Matrix(Direct &seqone, Direct &seqtwo, struct scoretp &param): origin(seqone), derived(seqtwo){ 
// Input parameter seqone is a reference to a Direct object holding one DNA sequence.
// Input parameter seqtwo is a reference to a Direct object holding another DNA sequence.
// Input parameter param is a reference to a struct scoretp variable holding the scoring parameters.

// The constructor sets the member reference origin to seqone, and
// the member reference derived to seqtwo,
// through the initialization list before the constructor body.
//
// The constructor sets the member rowind to the return value from getLength() of seqone.
// The constructor sets the member colind to the return value from getLength() of seqtwo.
// If both rowind and colind are zero, then prints an error message.
// Then it allocates space for each
// matrix in the mattp structure by calling get2dspace(). Next it computes,
// in reverse order, the three matrices by the dynamic programming algorithm
// for aligning the two sequences pointed to by getLength() of seqone
// that of seqtwo. The sequence in seqone is treated as the sequence A.
        rowind = seqone.getLength();
        colind = seqtwo.getLength();
        if(rowind == 0 && colind == 0){
            fatal("The length is not valid \n");
        }
        scoretp &paramt = param;
        computemats(origin,derived,paramt);
    }; // normal constructor
int** Matrix::get2dspace(int rowind, int colind){
// Allocates a 2-dimensional int array of (rowind + 1) by (colind + 1)
// and returns its memory address.
// Prints out an error message if rowind or colind is less than 0.
     int  j;
     if ( rowind < 0 || colind < 0 )
             fatal("get2darr: negative parameter\n");
     int** a2pt = new int*[rowind+1];
     for ( j = 0; j <= rowind; j++ ) {
         a2pt[j] = new int[colind+1];
     }

     return a2pt;
}
void Matrix::free2dspace(int rowind, int **arr){
// Deallocates a 2-dimensional int array of first dimension 
// (rowind + 1) by releasing the memory for arr[j] for each j
// and then releasing the memory for arr.
// Prints out an error message if rowind is less than 0.
     if ( rowind < 0 )
             fatal("get2darr: negative parameter\n");
     int i;
     for(i = 0; i <= rowind ; i++){
         delete[] arr[i];
     }
     delete[] arr;
}
Direct& Matrix::getOrigin() const{return origin;};  // returns origin.
Direct& Matrix::getDerived() const{return derived;}; // returns derived.
int Matrix::getRowInd() const{return rowind;};      // returns rowind.
int Matrix::getColInd() const{return colind;};      // returns rowind.
int** Matrix::getMat(char kind) const{
// If kind is 'S', then returns Smat.
// If kind is 'D', then returns Dmat.
// If kind is 'I', then returns Imat.
// Otherwise, prints out an error message.
    if (kind == 'D'){
        return Dmat;
    }
    else if (kind == 'I'){
        return Imat;
    }
    else if (kind == 'S'){
        return Smat;
    }
    else{
       fatal("getMat: The parameter is not valid, please specify it to 'D', 'I', or 'S'.\n");
    }
 return 0;
}

void Matrix::computemats(Direct &seqone, Direct &seqtwo, struct scoretp &param){

    int i;
    int j;
    int rind = rowind;
    int cind = colind;
    int gopen = param.gopen;
    int gext = param.gext;
    char * seqone_seq;
    char * seqtwo_seq;
    
    Dmat = get2dspace(rind,cind);
    Imat = get2dspace(rind,cind);
    Smat = get2dspace(rind,cind);
    seqone_seq = new char[rind+2];
    seqtwo_seq = new char[cind+2];
    strcpy(seqone_seq,seqone.getSeq());
    strcpy(seqtwo_seq,seqtwo.getSeq());
    
    this->Smat[rind][cind] = 0;
    this->Dmat[rind][cind] = this->Smat[rind][cind]-gopen;
    this->Imat[rind][cind] = this->Smat[rind][cind]-gopen;
    for(i=rind-1; i >=0; i--)
    {
       Dmat[i][cind] = Dmat[i+1][cind]-gext;
       Smat[i][cind] = Dmat[i][cind];
       Imat[i][cind] = Smat[i][cind]-gopen;
    }

    for(j=cind-1;j>=0; j--){
       Imat[rind][j] = Imat[rind][j+1]-gext;
       Smat[rind][j] = Imat[rind][j];
       Dmat[rind][j] = Smat[rind][j]-gopen;
    }

    for(i = rind-1; i >= 0; i --){

       Dmat[i][cind] = Dmat[i+1][cind]-gext;
       Smat[i][cind] = Dmat[i][cind];
       Imat[i][cind] = Smat[i][cind]-gopen;
        for(j = cind-1; j >= 0; j --){
            Dmat[i][j] = max((Dmat[i+1][j]-gext),(Smat[i+1][j]-gext-gopen));
            Imat[i][j] = max((Imat[i][j+1]-gext),(Smat[i][j+1]-gext-gopen));

            int diMax = max((Dmat[i][j]),(Imat[i][j]));
            char a = seqone_seq[i+1];
            char b = seqtwo_seq[j+1];
            int matchingS = matchingScore(a, b, param);
            int s = (Smat[i+1][j+1])+matchingS;
            Smat[i][j] = max((s),(diMax));
        }
    }
    delete[] seqone_seq;
    delete[] seqtwo_seq;
}

string Matrix::toString(char kind) const{
// Input parameter kind denotes matrix type: 'D', 'I', or 'S'.
// The function returns the matrix type and each value in the matrix
// in the form of a string.
// This function is for your own use to check on each matrix,
// so any format is OK.
    //std::vector<char> mat_str; 
    //unsigned char * mat_str = new unsigned char[];
    char buffer[1000];
    stringstream outstr;
    int i,j;
    int m = getRowInd();
    int n = getColInd();
    int** Mat = getMat(kind);
    outstr<< "The type of the matrix is " << kind << endl;
    for(i = 0; i <= m; i++){
        for (j = 0; j <= n ; j ++){
            sprintf(buffer, "%d ", Mat[i][j]);
            outstr << buffer;
        }
        sprintf(buffer, "\n");
        outstr << buffer;
    }
    return outstr.str();
}
// Frees heap memory.
Matrix::~Matrix(){
    free2dspace(rowind,Dmat);
    free2dspace(rowind,Imat);
    free2dspace(rowind,Smat);
}

