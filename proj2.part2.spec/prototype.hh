//@author: Siyu Lin
#ifndef PROTOTYPE_H
#define PROTOTYPE_H
#include <string.h>
#include <iostream> // provides objects like cin and cout for sending data
                    // to and from the standard streams input and output.
		    // These objects are part of the std namespace.
#include <cstdlib>  // has exit etc.
#include <fstream>  // file streams and operations
#include <sstream>  // string streams and operations
using namespace std; // a container for a set of identifiers.
                     // Because of this line, there is no need to use std::cout
#include <typeinfo> // This header file is included for using typeid.
#include <stdexcept>
#include <iomanip>

#include <stdio.h>
#include <string.h>
#include <ctype.h>  // A library with functions for testing characters.
struct scoretp  // a structure for scoring parameters
 { int  match;  // a positive score for a pair of identical DNA letters
   int  mismat; // a negative score for a pair of different DNA letters
   int  gopen;  // a negative score for a gap
   int  gext;   // a negative score for each letter in a gap
 };

struct Edit        // a structure for an edit operation
 { int   position; // a sequence position to which the edit operation is applied
   short indel;    // indel < 0, the edit is a deletion gap of length -indel;
                   // indel > 0, the edit is an insertion gap of length indel;
                   // indel = 0, the edit is a substitution.
 };

#endif
