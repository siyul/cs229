//@author: Siyu Lin
#include "Encoded.hh"
Encoded::Encoded(Alignment &obj): origin(obj.getOrigin()){
// Input parameter obj is a reference to an Alignment object holding an optimal
// alignment along with sequence and other information.

// The constructor initializes some members by calling public functions
// of Alignment from the object obj. Among these members are
// origin, the reference to the original sequence,
// editnum, the size of the array operation, and
// subinsertlen, the size of the array subinsertion.
// the name and length of the derived sequence.
// The constructor allocates memory to operation and subinsertion.
// Then it constructs the two arrays operation and subinsertion
// by follwing the method in Figure 2 of the project description.
// The construction uses top & mid & bot, the three rows of the optimal alignment,
// which are provided by the public functions of Alignment from the object obj.
    editnum = obj.getEditNum();
    subinsertlen = obj.getSubInsertLen();
    dname = obj.getDerived().getName();
    operation = new Edit[editnum];
    subinsertion = new char[subinsertlen+1];
    encodeOrigin(obj);
    deletion_gaps = 0;
    decodeOrigin(origin);
}
void Encoded::encodeOrigin(Alignment &obj){
    int i =  1; 
    int j = 1;
    int opind = 0;
    int snind = 0;
    int ind = 0;
    int alength = obj.getAlignLen();
    char * top = obj.getRow('T');
    char * mid = obj.getRow('M');
    char * bot = obj.getRow('B');
    char link = '|';
    char blank = ' ';
    for(ind = 0; ind < alength;){
        if(top[ind] == bot [ind] && mid[ind] == link ){
            i++;
            j++;
            ind++;
            continue;
        }
        else if ( top[ind] != bot[ind] && mid[ind] == blank ) {
            operation[opind].position = i;
            operation[opind].indel = 0;
            subinsertion[snind++] = bot[ind];
            i++;
            j++;
            ind++;
            opind++;
            continue;
        }
        else{
            int gaplen = 0;
            while(mid[ind+gaplen] == '-'){
                gaplen++;
            }
            operation[opind].position = i;
            if( top[ind] != blank && mid[ind] == '-'){
                i+= gaplen;
                operation[opind++].indel = -gaplen;
                ind += gaplen;
                //continue;
            } //Deletion gap
            else{
                j += gaplen;
                operation[opind++].indel = gaplen;
                for(; gaplen >= 1; gaplen--){
                    subinsertion[snind++] = bot[ind++];
                }
                //continue;
            }//Insetion Gap
        }
    }
    dlength  = j-1;
    subinsertion[snind] = '\0';
}
void Encoded::decodeOrigin(Direct &origin){
    int i = 1;
    int j = 1;
    int snind = 0;
    int opind = 0;
    derived = new char[dlength+2];
    char *originA = origin.getSeq();
    int m = origin.getLength();
    derived[0] = ' ';
    for(; opind<editnum; opind++){
        int oppos = operation[opind].position;
        int indel = operation[opind].indel;
        while(i < oppos){
            derived[j++] = originA[i++];
        }
        if(indel < 0){
            //printf("Operation %d :: Position: %d Indel: %d Deletion \n", opind, oppos, indel);
            i -= indel;
            deletion_gaps -= indel;
        }
        else if (indel ==0){
            //printf("Operation %d :: Position: %d Indel: %d Substitution: %c \n", opind, oppos, indel, subinsertion[snind]);
            i++;
            derived[j++] = subinsertion[snind++];
        }
        else{
            char * temp = new char[indel+1];
            int k = 0;
            for(; indel; indel--){
                temp[k++] = subinsertion[snind];
                derived[j++] = subinsertion[snind++]; 
            }
            temp[k] = '\0';
            //printf("Operation %d :: Position: %d Indel: %d Insertion: %s \n", opind, oppos, indel, temp);
            delete[] temp;
        }
     }
    while(i <= m){
        derived[j++] = originA[i++];
    }
    derived[dlength+1] = '\0';
}
int Encoded::getEditNum() const{
    return editnum;
}
struct Edit* Encoded::getOperation() const{
    return operation;
}
int Encoded::getSubInsertLen() const{
    return subinsertlen;
}
char* Encoded::getSubInsertion() const{
    return subinsertion;
}
int Encoded::getDLength() const{
    return dlength;
}
string Encoded::getDName() const{
    return dname;
}
Direct& Encoded::getOrigin() const {
    return origin;
}
char* Encoded::getDSeq() const {
    return derived;
}// deirves a sequence and turns it in a char array.
Encoded::~Encoded(){
    delete[] subinsertion;
    delete[] operation;
    delete[] derived;
}

string Encoded:: toString() const
// Generates and returns a string form of its contents.
// The information includes the following lines:
// Name of the encoded sequence: ...
// Length of the encoded sequence: ...
// Number of edit operations: ...
// Length of substitutions and insertions: ...
// Concatenation of subs and inserts in order: ...
//
// and each of the operations in the array operation.
{
    int i = 1;
    int j = 1;
    int snind = 0;
    int opind = 0;
    //int m = origin.getLength();
    char buffer[1000];
    stringstream outstr;
    sprintf(buffer, "Name of the encoded sequence:");
    outstr << buffer;
    outstr << dname;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"Length of the encoded sequence: %d", dlength);
    outstr << buffer;
    sprintf(buffer, "\n");
    outstr << buffer;
    sprintf(buffer,"Number of the edit operations:  %d \n", editnum);
    outstr << buffer;
    sprintf(buffer, "Length of substitutions and insertions   %d \n", subinsertlen);
    outstr << buffer;
    sprintf(buffer,"Concatenation of subs and inserts in order: %s \n", subinsertion);
    outstr << buffer;
    for(; opind<editnum; opind++){
        int oppos = operation[opind].position;
        int indel = operation[opind].indel;
        while(i < oppos){
            j++;
            i++;
        }
        if(indel < 0){
            sprintf(buffer,"Operation %d :: Position: %d Indel: %d Deletion \n", opind, oppos, indel);
            outstr << buffer;
            i -= indel;
        }
        else if (indel ==0){
            sprintf(buffer,"Operation %d :: Position: %d Indel: %d Substitution: %c \n", opind, oppos, indel, subinsertion[snind]);
            outstr << buffer;
            i++;
            j++;
            snind++;
        }
        else{
            char * temp = new char[indel+1];
            int k = 0;
            for(; indel; indel--){
                temp[k++] = subinsertion[snind];
                j++;
                snind++;
            }
            temp[k] = '\0';
            sprintf(buffer, "Operation %d :: Position: %d Indel: %d Insertion: %s \n", opind, oppos, k, temp);
            outstr << buffer;
            delete[] temp;
        }
     }
    return outstr.str();
}

bool Encoded::operator<=(Encoded &rightobj) const{
    if(this->getNumDiff() < rightobj.getNumDiff()){
        return true;
    }
    else if (this->getNumDiff() > rightobj.getNumDiff()){
        return false;
    }
    else{
        if(this->getSubInsertLen() <= rightobj.getSubInsertLen()){
            return true;
        }
        else {
            return false;
        }
    }

}
int Encoded:: getNumDiff() const{
    // total length of deletion gaps and insertion gaps plus the total number of substitutions, 
    //Get deletion gaps
    //int dele = deletion_gaps
    //Get insertion gaps and substitution gaps
    //int subins = subinsertlen;
    //printf("Subinsertlen is %d \n", getSubInsertLen());
    //printf("Deletion gap is %d \n", deletion_gaps);
    return deletion_gaps+subinsertlen;
}

Compressed::Compressed(Alignment &obj):Encoded(obj){
}
bool Compressed::operator<=(Encoded &rightobj) const{
    if(this->getNumDiff() < rightobj.getNumDiff()){
        return true;
    }
    else if (this->getNumDiff() > rightobj.getNumDiff()){
        return false;
    }
    else{
        if(this->getSubInsertLen() < rightobj.getSubInsertLen()){
            return true;
        }
        else if (this->getSubInsertLen() > rightobj.getSubInsertLen()) {
            return false;
        }
        else{
            if(strcmp(getSubInsertion(), rightobj.getSubInsertion()) <= 0 ){
                return true;
            }
            else{
                return false;
            }
        }
    }
}
Compressed::~Compressed(){
}
