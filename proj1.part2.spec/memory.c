/*
 * =====================================================================================
 *
 *       Filename:  memory.c
 *
 *    Description:  Memory allocation and free for project 1B in 14 Fall Com S 229
 *
 *        Version:  1.0
 *        Created:  10/19/2014 06:31:55 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Siyu Lin
 *         @author: Siyu Lin
 *   Organization:  Iowa State Univerisity
 *
 * =====================================================================================
 */


#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit()
#include <string.h> // A library with strcpy(), strcmp(), strlen()
#include "prototype.h" // includes a user-defined header file with double quotes.
#include "wrapper.h"    // includes a user-defined header file with double quotes.
void getseq(const char *fname, struct seqtp *seqrec){

   FILE *fp;
   int len;
   char sequence[MaxLineLen];
   int ch;
   int j;
   int lcount;
   long temp;
   len = 0;
   fp = ckopen(fname,"r");
   while((ch = fgetc(fp)) != '\n')
       len++;
   if(len<=1)
       fatal("The name is not valid! \n");
   rewind(fp);
   //Read the first line to the name
   //seqrec->name = (char *)malloc((len)*sizeof(char));
   seqrec->name = (char *)malloc((len+1)*sizeof(char));//\0 char..overflow? and the \n char
   //Skip the >
   fseek(fp,1,SEEK_SET);
   fgets(seqrec->name, MaxLineLen, fp);
   //fscanf(fp, "%s", seqrec->name);
   seqrec->name[len-1] = '\0';
   //char * npt = seqrec->name;
   //while(*npt++ != '\0'){
   //    if(*npt == '\n')
   //        printf("Name contains newline char \n");
   //}
   //free(npt);


   temp = ftell(fp);
   //Read the lines
   lcount=0;
   len = 0;
   while(!feof(fp)){
       ch = fgetc(fp);
       if(ch == '\n'){
           lcount++;
       }
       else{
           len++;
       }
   }

   
   fseek(fp,temp,SEEK_SET);
   seqrec->seq  = NULL;
   seqrec->seq = (char *)check_malloc((len+2)*sizeof(char));
   seqrec->seq[0] = ' '; 
   seqrec->seq[1] = '\0';
   //Concatenate the sequence and a blank space
   for(j = 0; j < lcount; j++){
       //fgets(sequence, len, fp);
       fscanf(fp,"%s",sequence);
       strcat(seqrec->seq, sequence);
   }

    j = 0;
    while(seqrec->seq[j++] != '\0');
    seqrec->slen = j-2;
    fclose(fp);
}
void **get2dspace(int rind, int cind){
     void  **a2pt;
     int  j;
     if ( rind < 0 || cind < 0 )
             fatal("get2darr: negative parameter\n");
     a2pt = check_malloc((rind+1) * sizeof(int * ));
     for ( j = 0; j <= rind;j++ ) 
         a2pt[j] = (int *) check_malloc( (cind+1) * sizeof(int) );
     return a2pt;
}
void freeseq(struct seqtp *seqrec){
    free(seqrec->name);
    seqrec->name = NULL;
    free(seqrec->seq);
    seqrec->seq = NULL;
    seqrec->slen = 0;
}
void freemat(struct mattp *matspt){
    int rind = matspt->rind;
    free2dspace(rind,matspt->Dpt); 
    free2dspace(rind,matspt->Ipt); 
    free2dspace(rind,matspt->Spt); 
}

void freealg(struct algtp *algopt){
    free(algopt->top);
    free(algopt->mid);
    free(algopt->bot);
    algopt->alen = 0;
    algopt->score = 0;
    algopt->top = NULL;
    algopt->mid = NULL;
    algopt->bot = NULL;
}
void free2dspace(int rind, int **arr){
     if ( rind < 0 )
             fatal("get2darr: negative parameter\n");
     int i;
     for(i = 0; i < rind+1 ; i++){
         free(arr[i]);
         arr[i] = NULL;
     }
     free(arr);
}

