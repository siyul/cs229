/*
 * =====================================================================================
 *
 *       Filename:  alignment.c
 *
 *    Description:  Computing the score of the local alignment and produce the local alignment 
 *
 *        Version:  1.0
 *        Created:  10/19/2014 06:33:10 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Siyu Lin
 *         @author: Siyu Lin
 *   Organization:  Iowa State University
 *
 * =====================================================================================
 */

#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit()
#include <string.h> // A library with strcpy(), strcmp(), strlen()
#include "prototype.h" // includes a user-defined header file with double quotes.
#include "wrapper.h"    // includes a user-defined header file with double quotes.

void computemats(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct mattp *matspt){
    int i;
    int j;
    int rind;
    int cind;
    inline int matchingScore(char x, char y) { 
        return x == y ? (*papt).match : (*papt).mismat;
    }
    (*matspt).rind = (*sonept).slen;
    rind = (*sonept).slen;
    (*matspt).cind = (*stwopt).slen;
    cind = (*stwopt).slen;
    (*matspt).score = 0;
    (*matspt).rowfirst = rind;
    (*matspt).colfirst = cind;

    (*matspt).Dpt = (int **)get2dspace(rind, cind); 
    (*matspt).Ipt = (int **)get2dspace(rind, cind); 
    (*matspt).Spt = (int **)get2dspace(rind, cind); 

    //Base cases
    (*matspt).Spt[rind][cind] = 0;
    (*matspt).Dpt[rind][cind] = -((*papt).gopen+(*papt).gext);
    (*matspt).Ipt[rind][cind] = -((*papt).gopen+(*papt).gext);
    //Initializing the last colomn
    //for(i=rind-1; i >=0; i--)
    //{
    //   (*matspt).Dpt[i][cind] = (*matspt).Dpt[i+1][cind]-(*papt).gext;
    //   (*matspt).Spt[i][cind] = (*matspt).Dpt[i][cind];
    //   (*matspt).Ipt[i][cind] = (*matspt).Spt[i][cind]-(*papt).gopen;
    //}


    //Initializing the last row
    for(j=cind-1;j>=0; j--){
       (*matspt).Dpt[rind][j] = -((*papt).gopen+(*papt).gext);
       (*matspt).Ipt[rind][j] = -((*papt).gopen+(*papt).gext);
       (*matspt).Spt[rind][j] = 0;
    }



    for(i = rind-1; i >= 0; i --){
       //(*matspt).Dpt[i][cind] = (*matspt).Dpt[i+1][cind]-(*papt).gext;
       (*matspt).Dpt[i][cind] = -((*papt).gopen+(*papt).gext);
       (*matspt).Spt[i][cind] = 0;
       (*matspt).Ipt[i][cind] = -((*papt).gopen+(*papt).gext);
       //(*matspt).Ipt[i][cind] = (*matspt).Spt[i][cind]-(*papt).gopen;
        for(j = cind-1; j >= 0; j --){
            (*matspt).Dpt[i][j] = max(((*matspt).Dpt[i+1][j]-(*papt).gext),((*matspt).Spt[i+1][j]-(*papt).gext-(*papt).gopen));
            (*matspt).Ipt[i][j] = max(((*matspt).Ipt[i][j+1]-(*papt).gext),((*matspt).Spt[i][j+1]-(*papt).gext-(*papt).gopen));

            int diMax = max(((*matspt).Dpt[i][j]),((*matspt).Ipt[i][j]));
            char a = (*sonept).seq[i+1];
            char b = (*stwopt).seq[j+1];
            int matchingS = matchingScore(a, b);
            int s = ((*matspt).Spt[i+1][j+1])+matchingS;
            (*matspt).Spt[i][j] = max((s),(diMax));
            if((*matspt).Spt[i][j] < 0)
                (*matspt).Spt[i][j] = 0;

            if((*matspt).score < (*matspt).Spt[i][j])
            {
                (*matspt).score = (*matspt).Spt[i][j];
                (*matspt).rowfirst = i;
                (*matspt).colfirst = j;
            }
        }
    }

}
void producealg(struct seqtp *sonept, struct seqtp *stwopt,  struct mattp *matspt, struct algtp *algopt){
    enum MatchType {S,D,I};
    
    enum MatchType curMat = S;
    //int i = 0;
    int i = (*matspt).rowfirst;
    //int j = 0;
    int j = (*matspt).colfirst;
    int m = (*matspt).rind;
    int n = (*matspt).cind;
    char link = '|';
    //int q;
    //int r;
    int qr_sum;
    if(m > 1){
         //q = -(matspt->Dpt[m][n]);
         //r = -((matspt->Dpt[m-1][n])+q);
         qr_sum = -matspt->Dpt[m][n];
    }
    if(n > 1){
         //q = -(matspt->Ipt[m][n]);
         //r = -((matspt->Ipt[m][n-1])+q);
         qr_sum = -matspt->Ipt[m][n];
    }

    (*algopt).top = (char *)check_malloc((m+n)*sizeof(char));
    (*algopt).mid = (char *)check_malloc((m+n)*sizeof(char));
    (*algopt).bot = (char *)check_malloc((m+n)*sizeof(char));
    //(*algopt).score = (*matspt).Spt[0][0]; 
    (*algopt).score = (*matspt).score; 
    (*algopt).alen = 0;
    while(i <= m && j <= n){
            char a = (*sonept).seq[i+1];
            char b = (*stwopt).seq[j+1];
            int current = (*algopt).alen;
        if(curMat == S){
            if (i == m || j == n || (*matspt).Spt[i][j] == 0)
                break;
            if((*matspt).Spt[i][j] == (*matspt).Dpt[i][j]){
                curMat = D;
                continue;
            }
            if((*matspt).Spt[i][j] == (*matspt).Ipt[i][j] ){
                curMat = I;
                continue;
            }
            algopt->top[current] = a; 
            algopt->bot[current] = b; 
            if(a == b){
                algopt->mid[current] = link; 
            }
            else{
                algopt->mid[current] = ' '; 
            }
            (*algopt).alen ++;

            i++;
            j++;
            continue;
        }
       if(curMat == D){
           //Append a, -

            algopt->top[current] = a; 
            algopt->mid[current] = '-'; 
            algopt->bot[current] = ' '; 
            (*algopt).alen ++;
           if(i == m-1 || (*matspt).Dpt[i][j] == (*matspt).Spt[i+1][j]-qr_sum){
               curMat = S;
           }
           i++;
           continue;
        }
      if(curMat == I){
           //Append -, b
            algopt->top[current] = ' '; 
            algopt->mid[current] = '-'; 
            algopt->bot[current] = b; 
            (*algopt).alen ++;
           if(j == n-1 || (*matspt).Ipt[i][j] == (*matspt).Spt[i][j+1]-qr_sum){
               curMat = S;
           }
           j++;
           continue;
      }
    }
    (*algopt).rowlast = i;
    (*algopt).collast = j;
    (*algopt).rowfirst = (*matspt).rowfirst;
    (*algopt).colfirst = (*matspt).colfirst;
}
