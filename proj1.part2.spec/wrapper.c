
/*
 * =====================================================================================
 *
 *       Filename:  wrapper.c
 *
 *    Description:  Helper functions in Project 1B
 *
 *        Version:  1.0
 *        Created:  10/26/2014 09:12:33 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin    
 *         @author: Siyu Lin 
 *   Organization: Iowa State University 
 *
 * =====================================================================================
 */
#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit()
#include <string.h> // A library with strcpy(), strcmp(), strlen()
#include "prototype.h" // includes a user-defined header file with double quotes.
#include "wrapper.h" // includes a user-defined header file with double quotes.

// Checks whether realloc() succeeds if amount is not 0.
void *check_realloc(void *ptr, size_t amount)
{
  void *tpt;
  // Returns the old memory block and allocates a new one in amount bytes.
  tpt = realloc(ptr, amount);

  /* Checks if it was successful. */
  if ( amount != 0 && tpt == NULL )
   { // prints a message to standard error device (console).
     fprintf(stderr, "No memory of %lu bytes\n", amount);
     printf("No memory of %lu bytes\n", amount);
     exit(1); // stops unexpectedly.
   }

  return tpt;
}

// Checks if malloc() succeeds.
void *check_malloc(size_t amount)
{
  void *tpt;
  /* Allocates a memory block in amount bytes. */
  tpt = malloc( amount );

  /* Checks if it was successful. */
  if ( tpt == NULL )
   { // prints a message to standard error device (console).
     fprintf(stderr, "No memory of %lu bytes\n", amount);
     printf("No memory of %lu bytes\n", amount);
     exit(1); // stops unexpectedly.
   }

  return tpt;
}

// Prints out an error message and stops.
void fatal(const char *msg)
{
     fprintf(stderr, "Error message: %s\n", msg);
 //  printf("Error message: %s\n", msg);
     exit(1); // stops unexpectedly.
}

// Opens file and checks for success.
// Argument name is a pointer to a string used as a file pathname.
// Argument mode is a pointer to a string used as an access mode.
// Three modes for text files: "r" for read, "w" for write, and "a" for append.
// For binary files, a "b" is appended like "rb", "wb", and "ab".
// Note that ckopen is a wrapper function for
//     FILE *fopen(const char *name, const char *mode);
//   which returns a pointer to a FILE.
FILE *ckopen(const char *name, const char *mode)
{
	FILE *fp;

	if ((fp = fopen(name, mode)) == NULL)
	{ fprintf(stderr, "Cannot open %s\n", name);
//	  printf("Cannot open %s\n", name);
          exit(1); // stops unexpectedly.
	}
	return(fp);
}
