
/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  main functions for Project 1B
 *
 *        Version:  1.0
 *        Created:  10/26/2014 09:12:33 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin    
 *         @author: Siyu Lin 
 *   Organization: Iowa State University 
 *
 * =====================================================================================
 */
#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit()
#include <string.h> // A library with strcpy(), strcmp(), strlen()
#include "prototype.h" // includes a user-defined header file with double quotes.
#include "wrapper.h"    // includes a user-defined header file with double quotes.

int main(int argc, char *argv[]) 
{
    char *seqAFile;
    char *seqBFile;
    struct seqtp *seqApt;
    struct seqtp *seqBpt;
    struct scoretp defaultConfig;
    struct mattp *mat;
    struct algtp *alg;

   if ( argc != 6 )
   { fprintf(stderr,"Usage: %s Seq1 Seq2 mismatch gap_open gap_extend\n\n", argv[0]);
     fprintf(stderr,"Seq1        file of one sequence in FASTA format\n");
     fprintf(stderr,"Seq2        file of one sequence in FASTA format\n");
     fprintf(stderr,"mismatch    a negative integer\n");
     fprintf(stderr,"gap_open    gap open penalty, a non-negative integer \n");
     fprintf(stderr,"gap_extend  gap extension penalty, a positive integer \n");
     // Each base match score is always 10.
     exit(1);
   }

     sscanf(argv[3], "%d", &(defaultConfig.mismat));
     if(defaultConfig.mismat >= 0){
         fatal("Mismatch should be negative \n");
     }
     sscanf(argv[4], "%d", &(defaultConfig.gopen));
     if(defaultConfig.gopen <= 0){
         fatal("Gap-open should be positive \n");
     }
    sscanf(argv[5], "%d", &(defaultConfig.gext));
     if(defaultConfig.gext <= 0){
         fatal("Gap-Extension should be positive \n");
     }
    printf("%10s %10s %10s %10s\n", "Match Score", "Mismatch Score", "Gap-open", "Gap-extension");
    defaultConfig.match = 10; 
    printf("%10d %10d %10d %10d\n", defaultConfig.match, defaultConfig.mismat, defaultConfig.gopen, defaultConfig.gext);
    //Write sequence A to seqtp struct
    //


    seqApt =(struct seqtp*)check_malloc(sizeof(struct seqtp));    
    seqAFile=argv[1];

    //Write sequence A to seqtp struct

    seqBpt =(struct seqtp*)check_malloc(sizeof(struct seqtp));    
    seqBFile=argv[2];
    getseq(seqAFile, seqApt);
    getseq(seqBFile, seqBpt);


    mat = (struct mattp*)check_malloc(sizeof(struct mattp));
    alg = (struct algtp*)check_malloc(sizeof(struct algtp));

    computemats(seqApt,seqBpt,&defaultConfig,mat);
    producealg(seqApt,seqBpt,mat,alg);
    outputalg(seqApt,seqBpt,&defaultConfig,alg);
    //outputmat('S',seqApt,seqBpt,(*mat).Spt);
    printf("Score is %d\n", mat->score);

    freeseq(seqApt);
    freeseq(seqBpt);
    free(seqApt);
    seqApt = NULL;
    free(seqBpt);
    seqBpt = NULL;
    freemat(mat);
    free(mat);
    mat = NULL;
    freealg(alg);
    free(alg);
    alg = NULL;

    return 0;
}

