/*
 * =====================================================================================
 *
 *       Filename:  output.c
 *
 *    Description:  Generating the output of the alignment, including the matrix and the alignment
 *
 *        Version:  1.0
 *        Created:  10/19/2014 06:34:06 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Siyu Lin
 *         @author:  Siyu Lin
 *   Organization: Siyu Lin 
 *
 * =====================================================================================
 */


#include <stdio.h>  // A C library with I/O functions like printf
#include <stdlib.h> // A library with malloc(), realloc(), free(), exit()
#include <string.h> // A library with strcpy(), strcmp(), strlen()
#include "prototype.h" // includes a user-defined header file with double quotes.
#include "wrapper.h"    // includes a user-defined header file with double quotes.
void outputmat(char kind, struct seqtp *sonept, struct seqtp *stwopt, int **Mpt){
    
    printf("The type of the Matrix is %c \n", kind);
    //Print the rows
    int m = (*sonept).slen;
    int n = (*stwopt).slen;
    int i, j;
    //Print the title
    printf("%s \n", (*stwopt).name);
    for(j = 0; j < n; j++){
        printf(" %c ", (*stwopt).seq[j]);
    }
    printf("\n");
    printf("Colomn");
    for(j = 0; j < n; j ++){
        printf("  %d  ", j);
    }

    printf("\n %s", (*sonept).name);
    for(i = 0; i < m; i++){
        printf(" %c ", (*sonept).seq[i]);
        printf("Row %d  ", i );
        for(j=0;j < n; j++){
            printf(" %d " , Mpt[i][j]);
        }
        printf("\n");
    }

}
void outputalg(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct algtp *algopt){
    int row = ((*algopt).alen)/70;
    int i = 0;
    //int currentPos = 1;
    int count_top = algopt->rowfirst+1;
    int count_bot = algopt->colfirst+1;
    printf("%10s%s\n", "Sequence A: ",sonept->name);
    printf("%10s%d\n", "Length: ", sonept->slen);
    printf("\n");

    printf("%10s%s\n", "Sequence B: ", stwopt->name);
    printf("%10s%d\n", "Length: ", stwopt->slen);
    printf("\n");

    printf("%15s%d \n ","Alignment Score: ", algopt->score);
    printf("%15s%d \n","Length: ", algopt->alen);
    printf("%15s%d \n","Start in A: ", algopt->rowfirst+1);
    printf("%15s%d \n","Start in B: ", algopt->colfirst+1);
    printf("%15s%d \n","End in A: ", algopt->rowlast);
    printf("%15s%d \n","End in B: ", algopt->collast);
    printf("\n");
    while(i <= row){

        printf("%9d \n", i*70+1);
       //Print 10 spaces
        printf("%9d ", count_top);
        //printf("%10s", "");
        //Print 70 charaters
        int j;
        int currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).top[i*70+j]);
            if((*algopt).top[i*70+j] != ' ')
                count_top++;
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                j = 71;
                break;
            }
        }

        printf("\n");
        currentChar = 0;
        printf("%10s", "");
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).mid[i*70+j]);
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                j = 71;
                break;
            }
        }
        printf("\n");
        printf("%9d ", count_bot);
        currentChar = 0;
        for(j = 0; j < 70; j ++ ){
            printf("%c", (*algopt).bot[i*70+j]);
            if((*algopt).bot[i*70+j] != ' ')
                count_bot++;
            currentChar++;

            if(i == row && currentChar == ((*algopt).alen%70)){
                i = row+1;
                j = 71;
                break;
            }
        }
        i++;
        printf("\n");
    }

}


