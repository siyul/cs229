
/*
 * =====================================================================================
 *
 *       Filename:  wrapper.h
 *
 *    Description:  Wrapper functions for Project 1B
 *
 *        Version:  1.0
 *        Created:  10/26/2014 09:12:33 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin    
 *         @author: Siyu Lin 
 *   Organization: Iowa State University 
 *
 * =====================================================================================
 */
// Conditional Inclusion
// The preprocessor directive tells the preprocessor to include
// the header file if the variable WRAPPER_G_H has not been defined.
// Once the header file is included, the variable variable WRAPPER_G_H
// is defined. So the header file cannot be included again.

#ifndef WRAPPER_G_H  // A header guard is introduced to avoid including
#define WRAPPER_G_H  // this header file twice in any source code file.

void fatal(const char *msg);
FILE *ckopen(const char *name, const char *mode);
void *check_malloc(size_t amount);
void *check_realloc(void *ptr, size_t amount);

#endif
