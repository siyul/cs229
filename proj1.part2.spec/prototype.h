/*
 * =====================================================================================
 *
 *       Filename:  prototype.h
 *
 *    Description:  It contains all the new function prototypes for projecect 1B in CS229.
 *
 *        Version:  1.0
 *        Created:  10/15/2014 09:12:33 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author: Siyu Lin    
 *         @author: Siyu Lin 
 *   Organization: Iowa State University 
 *
 * =====================================================================================
 */
#ifndef PROTOTYPE_H_   /*  Include guard */
#define PROTOTYPE_H_   
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define MaxLineLen 1000
struct seqtp   // a structure for three features of a sequence
 { char *name; // pointer to a string for keeping the name of a sequence
   char *seq;  // pointer to a char array for storing the sequence
   int  slen;  // the length of the sequence = char array length - 1,
 };            // where seq[1], seq[2], ... seq[n] are used
               // to keep the letters of a sequence of length n.

struct mattp    // a structure for three matrices
 {  int  rind;  // max row index
    int  cind;  // max column index
    int  score; // the score of an optimal local alignment
    int  rowfirst; // first row position of the alignment
    int  colfirst;// first column position of the alignment
    int  **Dpt; // an int pointer to 2-dimensional array D[rind + 1][cind + 1]
    int  **Ipt; // an int pointer to 2-dimensional array I[rind + 1][cind + 1]
    int  **Spt; // an int pointer to 2-dimensional array S[rind + 1][cind + 1]
 };

struct scoretp  // a structure for scoring parameters
 { int  match;  // a positive score for a pair of identical DNA letters
   int  mismat; // a negative score for a pair of different DNA letters
   int  gopen;  // a negative score for a gap
   int  gext;   // a negative score for each letter in a gap
 };

struct algtp    // a structure for an optimal alignment  
 { int  score;  // the score of the alignment
   int  alen;   // the length of the alignment
   char *top;   // pointer to a char array for the top row of the alignment
   char *mid;   // pointer to a char array for the mid row of the alignment
   char *bot;   // pointer to a char array for the bottom row of the alignment
              // each array is of length alen.
   int  rowfirst; // first row position of the alignment
   int  colfirst; // first column position of the alignment
   int  rowlast;  // last row position of the alignment
   int  collast;  // last column position of the alignment 
 };

//void fatal(const char *msg);
//FILE *ckopen(const char *name, const char *mode);
//void *check_malloc(size_t amount);
//void *check_realloc(void *ptr, size_t amount);
void **get2dspace(int rind, int cind);
void computemats(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct mattp *matspt);
void outputmat(char kind, struct seqtp *sonept, struct seqtp *stwopt, int **Mpt);
void freeseq(struct seqtp *seqrec);
void free2dspace(int rind, int **arr);
void freemat(struct mattp *matspt);
void producealg(struct seqtp *sonept, struct seqtp *stwopt, struct mattp *matspt, struct algtp *algopt);
void freealg(struct algtp *algopt);
void outputalg(struct seqtp *sonept, struct seqtp *stwopt, struct scoretp *papt, struct algtp *algopt);
void getseq(const char *fname, struct seqtp *seqrec);


#endif
